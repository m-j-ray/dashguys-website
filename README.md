# dashguys-website

[website for dashguys](https://m-j-ray.gitlab.io/dashguys-website/)

## Testing changes locally

Changes to the site can be quickly tested and previewed locally by cloning this repo and running the following command at the root of the repo. Hugo must be installed locally.

```
hugo server -w
```

Once running, the site can be previewed at: http://localhost:1313/dashguys-website/

## Carousel

The carousel is the slide show box at the top of the home page.

- Change carousel background:

  Depending on which style (color) you've selected, edit the corrisponding css file: `/static/css/style.<color>.css`

- Add/Remove slides from carousel:

  Each slide is a yaml file located in `/data/carousel/`

## Features

The features are the list of icons with text below the carousel on the home page.

- Add/Remove features from list:

  Each feature is a yaml file located in `/data/features/`

- Change feature icons:

  The icons used for features by this theme come from [fontawesome.com](https://fontawesome.com). Find a free icon and update the `icon:` value with that of the new icon name.

## Testimonials

- Add/Remove testimonials from list:

  Each feature is a yaml file located in `/data/testimonials/`

- Change tesimonials background:

  Depending on which style (color) you've selected, edit the corrisponding css file: `/static/css/style.<color>.css`

## Background bar (see more section)

The background bar is the section below testimonials that contains a static scrolling image, icon, text and a button.

- Change Background bar icon, text and a button:

  These can all be changed within `config.toml`. Optionally the entire section can also be removed/disabled.

    ```toml
    [params.see_more]
        enable = true
        icon = "far fa-file-alt"
        title = "Do you want to see more?"
        subtitle = "We have prepared for you more than 40 different HTML pages, including 5 variations of homepage."
        link_url = "#"
        link_text = "Check other homepages"
    ```

- Change Background bar image

  Depending on which style (color) you've selected, edit the corrisponding css file: `/static/css/style.<color>.css`

## Clients (Favorites)

The clients section does not have to be used for clients, but that is what its called. It is located below the blog entries.

- Add/Remove "clients" from list:

  Each feature is a yaml file located in `/data/clients/`

- Change "client" image

  Client images are located in `/static/img/clients/`
